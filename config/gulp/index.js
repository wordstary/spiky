import gulp from 'gulp';
import fs from 'fs';
import scriptFilter from './util/scriptFilter';

const tasks = fs.readdirSync('./config/gulp/tasks/').filter(scriptFilter);


tasks.forEach((task) => {
	require('./tasks/' + task);
});