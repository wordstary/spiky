import gulp from 'gulp';
import del from 'del';

// Remove our temporary files
gulp.task('clean:tmp', done => {
		del(['tmp']).then(() => done());
	}
);