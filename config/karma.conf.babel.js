/* eslint-env node */

import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import typescript from 'rollup-plugin-typescript';
import istanbul from 'rollup-plugin-istanbul';

module.exports = function(config) {

	const configuration = {
		frameworks: ['mocha', 'sinon-chai', 'expect'],

		basePath: '../',

		files: [
			'test/browser-tests/**/*browser.js',
			'test/node-tests/**/*node.js'
			//	{pattern: 'src/**/*.ts', included: false, watched: false},
			//  {pattern: 'test/**/*.ts', included: false, watched: false}
		],

		preprocessors: {
			'src/**/*.js': ['rollup'],
			'test/browser-tests/**/*browser.js': ['rollup'],
			'test/node-tests/**/*node.js': ['rollup']
		},
		rollupPreprocessor: {
			rollup: {
				plugins: [
					typescript(),
					babel({
						presets: ['es2015-rollup'],
						babelrc: false
					}),
					nodeResolve({
						jsnext: true,
						main: true
					}),
					istanbul({
						exclude: ['test/**/*.js']
					})
				]
			},
			bundle: {
				intro: '(function() {',
				outro: '})();',
				sourceMap: 'inline'
			}
		},
		coverageReporter: {
			reporters: [{

				type: 'text',
				dir: '../coverage'
			}, {
				type: 'lcov',
				dir: '../coverage'
			}]
		},

		reporters: ['mocha', 'coverage'],

		port: 9876,
		browserDisconnectTimeout : 20000, // ms
		browserDisconnectTolerance : 2, // times
		browserNoActivityTimeout : 40000, // ms
		colors: true,

		logLevel: config.LOG_INFO,

		autoWatch: false,

		singleRun: true,

		// change Karma's debug.html to the mocha web reporter
		client: {
			mocha: {
				reporter: 'html'
			}
		}
	};

	// gives a delay
	config.set(configuration);
};